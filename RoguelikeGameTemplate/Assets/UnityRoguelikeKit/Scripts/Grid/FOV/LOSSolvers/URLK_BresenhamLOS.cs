using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

//DISCLAIMER:
//This is a port of Eben Howard's SquidLib (in Java).
//

/**
 * A Bresenham-based line-of-sight algorithm.
 *
 * @author Eben Howard - http://squidpony.com - howard@squidpony.com
 * @author P�l Trefall - URLK - admin@ptrefall.com
 */
namespace URLK
{
    public class BresenhamLOS : LOSSolver
    {
        private Queue<Vector2> lastPath = new Queue<Vector2>();

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety,
                                   float force, float decay, RadiusStrategy radiusStrategy)
        {
            var path = Bresenham.line2D(startx, starty, targetx, targety);
            lastPath = new Queue<Vector2>(path); //save path for later retreival
            
            float currentForce = force;
            while(path.Count > 0)
            {
                var p = path.Dequeue();
                if (p.x == targetx && p.y == targety)
                {
                    return true; //reached the end 
                }
                if (p.x != startx || p.y != starty)
                {
                    //don't discount the start location even if on resistant cell
                    currentForce *= (1 - resistanceMap[Convert.ToInt32(p.x), Convert.ToInt32(p.y)]);
                }
                double radius = radiusStrategy.radius(startx, starty, p.x, p.y);
                if (currentForce - (radius*decay) <= 0)
                {
                    return false; //too much resistance
                }
            }
            return false; //never got to the target point
        }

        public Queue<Vector2> getLastPath()
        {
            return lastPath;
        }

        public bool isReachable(float[,] resistanceMap, int startx, int starty, int targetx, int targety)
        {
            return isReachable(resistanceMap, startx, starty, targetx, targety, float.MaxValue, 0f, new BasicRadiusStrategy(BasicRadiusStrategy.STRATEGY.CIRCLE));
        }
    }
}
