using System.Collections.Generic;
using UnityEngine;
using System;
using Boomlagoon.JSON;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class GameScreenData : IData
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public int get_active_game_screen()
    {
        var value = get_value("active_screen", "JSON/Screens/game_screen");
        if (value.Type != JSONValueType.Number)
        {
            //We could throw an exception here, but then the game would crash out...
            //So, we rather just log the error and return a default value of 0.
            Debug.LogError("active_screen in JSON/Screens/game_screen was not a number!");
            return 0;
        }

        return Convert.ToInt32(value.Number);
    }

    public override void reload()
    {
        reload_file("JSON/Screens/game_screen");
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    protected override void load_file()
    {
        load_file("JSON/Screens/game_screen");
    }
}
