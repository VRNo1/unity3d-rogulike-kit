
using UnityEngine;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class GameScreen : IScreen
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    private GameScreenData data;
    private GameObject player;

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                       C O N S T R U C T O R
    public GameScreen(Game game)
        : base(game)
    {
        data = new GameScreenData();
    }

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public override IScreen set_active()
    {
        //We might have been to a menu screen, only to return to game screen... 
        //so no reason to completely load that scene again!
        if (Application.loadedLevel != data.get_active_game_screen())
            Application.LoadLevel(data.get_active_game_screen());

        post_level_load();
        return this;
    }

    public override void update(float elapsed_time)
    {
        if(Input.anyKeyDown)
            on_key_down();
        else if(Input.anyKey)
            on_key();
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    private void post_level_load()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
            throw new System.InvalidOperationException("Couldn't find Player tagged GameObject in scene!");
    }

    private void on_key_down()
    {
        
    }

    private void on_key()
    {
        
    }
}
