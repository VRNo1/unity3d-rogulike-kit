
using UnityEngine;
using System.Collections;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public abstract class IScreen
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S
    protected Game game;

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public abstract void update(float elapsed_time);

    public virtual IScreen set_active()
    {
        return this;
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    protected IScreen(Game game)
    {
        this.game = game;
    }
}
