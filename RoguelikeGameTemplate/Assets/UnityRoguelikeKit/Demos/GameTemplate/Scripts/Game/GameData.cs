using System.Collections.Generic;
using UnityEngine;
using System;
using Boomlagoon.JSON;

//----------------------------------------------------------------- 
//                                           D E S C R I P T I O N
//
//
public class GameData : IData
{
    //------------------------------------------------------------- 
    //                                           V A R I A B L E S

    //------------------------------------------------------------- 
    //                                         P R O P E R T I E S

    //------------------------------------------------------------- 
    //                                   F U N C T I O N A L I T Y
    public string get_start_screen()
    {
        var value = get_value("start_screen", "JSON/game");
        if (value.Type != JSONValueType.String)
        {
            //We could throw an exception here, but then the game would crash out...
            //So, we rather just log the error and return a default value of string.Empty.
            Debug.LogError("start_screen in JSON/game was not a string!");
            return string.Empty;
        }

        return value.Str;
    }

    public override void reload()
    {
        reload_file("JSON/game");
    }

    //------------------------------------------------------------- 
    //                   P R I V A T E   F U N C T I O N A L I T Y
    protected override void load_file()
    {
        load_file("JSON/game");
    }
}
